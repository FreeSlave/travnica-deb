#! /bin/sh

if [ -z "$RENPY_SCREENSHOT_PATTERN" ]
then
    PICTURES=$HOME/Pictures
    if [ ! -d "$PICTURES" ]
    then
        install -d -- $PICTURES
    fi
    RENPY_SCREENSHOT_PATTERN=$PICTURES/travnica%04d.png
    export RENPY_SCREENSHOT_PATTERN
fi

/usr/games/renpy /usr/share/games/renpy/travnica/game
