This repository contains files to make .deb package for Travnica, RenPy game by [Moonworks](http://moonworks.ru/).

## На русском

Особенности:

* Игра добавляется в список приложений и должна появиться в меню рабочего столa.
* Игра добавляется в стандартное меню Debian.
* Пакет содержит мануал на русском и английском языках. Читать с помощью **man travnica**.
* Пакет содержит документацию. Обычно вы можете найти её в /usr/share/doc или просмотреть с помощью специальной программы (например, dwww)
* Скриншоты сохраняются в папке ~/Pictures

Заметки:
Так как пакет зависит от **renpy** из системых репозиториев, вы должны удостовериться, что имейте достаточно новую версию. В оригинале игра использует 6.18.3, но тесты показали, что она должна работать по крайней мере на 6.17.6.

Соответственно, подходящие дистрибутивы:

* Debian jessie и выше
* Ubuntu utopic и выше

Установка через терминал:

    sudo apt-get install renpy
    sudo dpkg -i travnica_1.1-1_all.deb

## In english

Features:

* Adds the game entry to application list and therefore should be visible in the desktop menu as described by [Desktop Menu Specification](http://standards.freedesktop.org/menu-spec/latest/)
* Adds the game entry to Debian menu as described by [Debian Menu System ](https://www.debian.org/doc/packaging-manuals/menu.html/)
* Installs man pages (english and russian), so you can call **man travnica** to read manual page.
* Installs documentation. Usually you can find it in /usr/share/doc or view with some specific utility (like dwww).
* Screenshots are saved in the ~/Pictures

Notes:
As long as this package depends on **renpy** from system repositories, you should make sure it has fairly new version. While original game uses renpy 6.18.3, tests shown it works at least on 6.17.6.

Therefore appropriate distros are:

* Debian jessie and higher
* Ubuntu utopic and higher

Install via terminal:

    sudo apt-get install renpy
    sudo dpkg -i travnica_1.1-1_all.deb
