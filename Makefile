GAME=game
NAME=travnica
STARTER=$(NAME).sh
DESKTOP=$(NAME).desktop
ICON=$(NAME).xpm

PREFIX=$(DESTDIR)/usr
INSTALL_GAMES=$(PREFIX)/games
INSTALL_SHARE=$(PREFIX)/share
INSTALL_RENPY=$(INSTALL_SHARE)/games/renpy
INSTALL_GAME=$(INSTALL_RENPY)/$(NAME)
INSTALL_PIXMAPS=$(INSTALL_SHARE)/pixmaps
INSTALL_APP=$(INSTALL_SHARE)/applications

all:

install:
	@echo $(DESTDIR)
	install -d $(INSTALL_GAMES) $(INSTALL_GAME) $(INSTALL_PIXMAPS) $(INSTALL_APP)
	cp -r $(GAME) $(INSTALL_GAME)
	install $(STARTER) $(INSTALL_RENPY)
	install -m644 readme_1_1_en.txt $(INSTALL_GAME)
	install -m644 readme_1_1_eng.pdf $(INSTALL_GAME)
	install -m644 readme_1_1_rus.txt $(INSTALL_GAME)
	install -m644 readme_1_1_rus.pdf $(INSTALL_GAME)
	install -m644 $(ICON) $(INSTALL_PIXMAPS)
	install $(DESKTOP) $(INSTALL_APP)
	ln -rfs $(INSTALL_RENPY)/$(STARTER) $(INSTALL_GAMES)/$(NAME)
